json.extract! log, :id, :syohai, :roll, :survive, :memo, :created_at, :updated_at
json.url log_url(log, format: :json)