class CreateLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :logs do |t|
      t.text :syohai
      t.text :roll
      t.text :survive
      t.text :memo

      t.timestamps
    end
  end
end
